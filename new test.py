import subprocess
from graphviz import Digraph
import re

def run_git_command(command, repo_path):
    """你要运行Git命令并返回输出结果。"""
    try:
        result = subprocess.run(command, capture_output=True, text=True, check=True, cwd=repo_path)
        return result.stdout.strip()
    except subprocess.CalledProcessError as e:
        print(f"Error running command '{' '.join(command)}': {e.stderr.strip()}")
        return None

def parse_git_log(log_output):
    """解析git log的输出，返回节点、边、合并节点和分支信息。"""
    nodes = []
    edges = []
    merge_nodes = []  # 存储合并节点
    branches = {}  # 存储分支信息
    current_node = None
    for line in log_output.split('\n'):
        if 'commit' in line:  # 假设每个提交都以'commit'开始
            parts = line.split()
            commit_hash = parts[1]
            if commit_hash not in nodes:
                nodes.append(commit_hash)
                current_node = commit_hash
        elif 'Merge:' in line:  # 合并提交
            merge_nodes.append(current_node)
        elif line.strip().startswith('Author:'):
            # 可以在这里解析作者信息，如果需要
            pass
        elif line.strip().startswith('Date:'):
            # 可以在这里解析日期信息，如果需要
            pass
        else:
            # 这里处理提交消息和其他可能的信息
            pass

        # 假设边的信息可以通过某种方式从输出中解析出来
        # 例如，通过解析父提交的信息
        # edges.append((parent_commit, commit_hash))

    # 模拟边的信息，实际应根据具体输出解析
    # for i in range(1, len(nodes)):
    #     edges.append((nodes[i-1], nodes[i]))

    return nodes, edges, merge_nodes, branches

def generate_graph(repo_path):
    """生成提交历史图形并保存为PNG文件。"""
    log_output = run_git_command(['git', 'log', '--all', '--graph', '--decorate', '--oneline'], repo_path)
    if not log_output:
        print("Failed to get git log.")
        return

    nodes, edges, merge_nodes, branches = parse_git_log(log_output)

    dot = Digraph(comment='Git Commit History')

    # 添加节点
    for node in nodes:
        if node in merge_nodes:
            dot.node(node, node[:7], shape='diamond', color='red')  # 合并节点
        else:
            dot.node(node, node[:7])  # 普通提交节点

    # 添加边
    for edge in edges:
        dot.edge(*edge)



    # 保存图形为PNG文件
    dot.render('git_history_all_branches', format='png', view=True)

repo_path = 'C:/Users/axin/Desktop/test 3/new-test'  # 更新为你的仓库路径
generate_graph(repo_path)


